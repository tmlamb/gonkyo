PROJECT_NAME := "gonkyo"
PKG := "github.com/tmlamb/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all deps updatedeps testdeps updatetestdeps build deploy clean test coverage coverhtml lint race msan help

all: deps testdeps lint test race coverage build

lint:
	golint -set_exit_status ${PKG_LIST}

test:
	go test -short ${PKG_LIST}

race: deps
	go test -race -short ${PKG_LIST}

msan: deps
	go test -msan -short ${PKG_LIST}

coverage:
	go test -cover ${PKG_LIST}

coverhtml:
	go test -coverprofile cover.out 
	go tool cover -html=cover.out -o cover.html

deps:
	go get -v -d ./...
	go get -u github.com/golang/lint/golint

updatedeps:
	go get -d -v -u -f ./...
	go get -u github.com/golang/lint/golint

testdeps:
	go get -d -v -t ./...

updatetestdeps:
	go get -d -v -t -u -f ./...

build: deps
	mkdir -p bin
	go build -o bin/server -i -v $(PKG)/server

deploy: build
	sudo service gonkyo stop
	cp bin/server /usr/local/gonkyo
	sudo service gonkyo start

clean:
	rm -r bin
