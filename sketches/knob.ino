#include <remote_knob.pb.h>

#include <pb_common.h>
#include <pb.h>
#include <pb_encode.h>
#include <pb_decode.h>

#include <ESP8266WiFi.h>

#include <Encoder.h>

const char* ssid     = "TGXQ";
const char* password = "changeit";
const char* addr     = "10.79.0.91";
const uint16_t port  = 10101;

Encoder knob(12, 13);

// setup WIFI and sensor
void setup() {
  Serial.begin(115200);
  delay(10);
  Serial.println("Starting Up the dial");
  Serial.println();
  Serial.print("Setting up WIFI for SSID ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("WiFi connected, ");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  WiFiClient client;
  Serial.println("");
  while (!client.connect(addr, port)) {
    Serial.println("Connection to Remote Knob service failed");
    Serial.println("Trying against in 750ms...");
    delay(750);
  } 
  Serial.println("Connection to Remote Knob service successful");
  client.stop();
}

long position  = -999;

void loop() {
  long newPosition;
  newPosition = (knob.read()/4);
  if (newPosition != position) {
    //Serial.print("position = ");
    //Serial.println(newPosition);
    
    remoteknob_Rotation rotation = remoteknob_Rotation_init_zero;
    rotation.delta = position - newPosition;
    sendRotation(rotation);
    
    position = newPosition;
  }
}

void sendRotation(remoteknob_Rotation r) {
  WiFiClient client;
  while (!client.connect(addr, port)) {
    Serial.println("Connection to Remote Knob service failed");
    Serial.println("Trying against in 750ms...");
    delay(750);
    return;
  } 
  uint8_t buffer[128];
  pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
  
  if (!pb_encode(&stream, remoteknob_Rotation_fields, &r)){
    Serial.println("failed to encode rotation proto");
    Serial.println(PB_GET_ERROR(&stream));
    return;
  }
  
  //Serial.print("sending rotation...");
  //Serial.println(r.delta);
  client.write(buffer, stream.bytes_written);
}