package client

import (
	"flag"
	"log"
	"fmt"
	"net/http"
	"time"
)

var (
	addr, port string
)

// ReceiverClient : manages access to the integra library
type ReceiverClient struct {
	netClient http.Client
	denonApiUrl string
}

// NewClient : provides a new instance of the ReceiverClient
func NewClient() *ReceiverClient {
	flag.StringVar(&addr, "chost", "10.79.0.9", "receiver host")
	flag.StringVar(&port, "cport", ":8080", "receiver port")

	client := http.Client{
		Timeout: time.Millisecond * 100,
	}

	c := &ReceiverClient{
		netClient: client,
		denonApiUrl: "http://" + addr + port,
	}

	resp, err := c.netClient.Get(c.denonApiUrl + "/goform/Deviceinfo.xml")
    if err != nil {
        log.Fatal(err)
    }

	fmt.Println("Denon API Connection Successful: ", resp)

	return c
}

// AdjustVolume : uses the integra library to adjust the volume up or down by the delta amount
func (c ReceiverClient) AdjustVolume(delta int32) {
	if (delta > 0) {
		_, err := c.netClient.Get(c.denonApiUrl + "/goform/formiPhoneAppDirect.xml?MVUP")
    	if err != nil {
        	log.Fatal(err)
    	}
	} else if (delta < 0) {
		_, err := c.netClient.Get(c.denonApiUrl + "/goform/formiPhoneAppDirect.xml?MVDOWN")
    	if err != nil {
        	log.Fatal(err)
    	}
	}
}