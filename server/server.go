package main

import (
	"flag"
	"log"
	"net"
	"os"

	rk "github.com/tmlamb/protorepo/remoteknob"
	"github.com/tmlamb/gonkyo/client"
	"github.com/golang/protobuf/proto"
)

var (
	addr, network string
)

func main() {

	client := client.NewClient()

	// setup flags
	network = "tcp"
	flag.StringVar(&addr, "eport", ":10101", "service endpoint port")
	flag.Parse()

	ln, err := net.Listen(network, addr)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	defer ln.Close()

	log.Printf("Remote Knob Service started: (%s) %s\n", network, addr)

	// connection loop
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
			conn.Close()
			continue
		}
		//log.Println("Connected to ", conn.RemoteAddr())
		go handleConnection(conn, client)
	}
}

func handleConnection(conn net.Conn, client *client.ReceiverClient) {
	defer func() {
		//log.Println("INFO: closing connection")
		if err := conn.Close(); err != nil {
			log.Println("error closing connection:", err)
		}
	}()

	buf := make([]byte, 1024)

	n, err := conn.Read(buf)
	if err != nil {
		log.Println(err)
		return
	}
	if n <= 0 {
		log.Println("no data received")
		return
	}

	var rotation rk.Rotation
	if err := proto.Unmarshal(buf[:n], &rotation); err != nil {
		log.Println("failed to unmarshal:", err)
		return
	}

	//log.Println(rotation.GetDelta())

	client.AdjustVolume(rotation.GetDelta());
}
